<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImgConvertController extends Controller
{
    public function convertImg(Request $request){
        $image = $request->file('image');
        $ext = 'png';
        $imageConvert = \Image::make($image->getRealPath())->stream($ext,100);
        \Storage::put('image/'.uniqid().'.'.$ext,$imageConvert);
        return redirect()->back()->withSuccess('Image convert successfully.');
    }
}
