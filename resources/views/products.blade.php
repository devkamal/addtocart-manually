@extends('layout')
    
@section('content')
     
<div class="row">
    @foreach($products as $product)
        <div class="col-xs-18 col-sm-6 col-md-4" style="margin-top:10px;">
            <div class="img_thumbnail productlist">
                <img src="{{ asset('img') }}/{{ $product->photo }}" class="img-fluid">
                <div class="caption">
                    <h4>{{ $product->product_name }}</h4>
                    <p>{{ $product->product_description }}</p>
                    <p><strong>Price: </strong> ${{ $product->price }}</p>
                    <p class="btn-holder"><a href="{{ route('add_to_cart', $product->id) }}" class="btn btn-primary btn-block text-center" role="button">Add to cart</a> </p>
                </div>
            </div>
        </div>
    @endforeach
</div>

<hr>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <div class="card">
            <div class="col-md-12 text-center mt-4">
                <h1>Image Converter Portion:</h1>
                @if(session('success'))
                    <div class="alert alert-success">{{session('success')}}</div>
                @endif
                <form action="{{ route('convert_image') }}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="imgconvert">
                        <label for="image">Image convert</label>
                        <input type="file" class="form-control" name="image">
                    </div>
                    <button type="submit" class="btn btn-primary mt-4">Upload Image for Convert</button>
                </form>
            </div>
        </div>
        </div>
    </div>
</div>
     
@endsection